import { Component, OnInit } from '@angular/core';
import { ConexionService } from '../../services/conexion.service';
import { isEmpty } from 'rxjs/operators';


@Component({
  selector: 'app-lista-add',
  templateUrl: './lista-add.component.html',
  styleUrls: ['./lista-add.component.css']
})
export class ListaAddComponent implements OnInit {

  item: any = {
    name: ''
  }

  constructor(private servicio: ConexionService) { }

  ngOnInit() {
  }

  agregar(){
    if (this.item.name != '') {
      this.servicio.adagregarItem(this.item);
      this.item.name = '';
    }    
  }
}
